package com.example.graphql.demo.todo.converter;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

import com.example.graphql.demo.todo.dto.TodoDto;
import com.example.graphql.demo.todo.dto.TodoItemDto;
import com.example.graphql.demo.todo.entity.Todo;

@Component
@RequiredArgsConstructor
public class TodoDtoConverter implements Converter<Todo, TodoDto> {

    private final TodoItemDtoConverter todoItemDtoConverter;

    @Override
    public @NonNull TodoDto convert(Todo source) {
        List<TodoItemDto> todoItems = source.getItems().stream().map(todoItemDtoConverter::convert).toList();
        return new TodoDto(source.getId(), source.getTitle(), source.isMarkAsComplete(), todoItems);
    }
}