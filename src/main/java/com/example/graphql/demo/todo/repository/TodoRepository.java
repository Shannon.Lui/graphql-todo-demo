package com.example.graphql.demo.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.graphql.demo.todo.entity.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo,Long> {

}
