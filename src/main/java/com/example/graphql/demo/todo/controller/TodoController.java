package com.example.graphql.demo.todo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.example.graphql.demo.todo.dto.TodoDto;
import com.example.graphql.demo.todo.service.TodoService;

@RestController
@RequestMapping(value = "/todo")
@RequiredArgsConstructor
public class TodoController {

    private final TodoService todoService;

    @QueryMapping
    public List<TodoDto> getAllTodo() {
        return todoService.getAllTodos();
    }

}
