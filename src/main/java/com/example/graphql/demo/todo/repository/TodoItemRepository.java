package com.example.graphql.demo.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.graphql.demo.todo.entity.TodoItem;

@Repository
public interface TodoItemRepository extends JpaRepository<TodoItem,Long> {

}
