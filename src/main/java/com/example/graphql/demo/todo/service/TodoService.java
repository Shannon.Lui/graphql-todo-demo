package com.example.graphql.demo.todo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import com.example.graphql.demo.todo.converter.TodoDtoConverter;
import com.example.graphql.demo.todo.dto.TodoDto;
import com.example.graphql.demo.todo.repository.TodoItemRepository;
import com.example.graphql.demo.todo.repository.TodoRepository;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository todoRepository;
    private final TodoItemRepository todoItemRepository;
    private final TodoDtoConverter todoDtoConverter;


    public List<TodoDto> getAllTodos() {
        return todoRepository.findAll().stream().map(todoDtoConverter::convert).toList();
    }

}
