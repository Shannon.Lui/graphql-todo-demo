package com.example.graphql.demo.todo.converter;

import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.graphql.demo.todo.dto.TodoItemDto;
import com.example.graphql.demo.todo.entity.TodoItem;

@Component
public class TodoItemDtoConverter implements Converter<TodoItem, TodoItemDto> {

    @Override
    public @NonNull TodoItemDto convert(TodoItem source) {
        return new TodoItemDto(source.getId(), source.getNotes(), source.getDueDate(), source.isMarkAsComplete(), source.getCompletionDate(), source.getCreatedAt(), source.getUpdatedAt());
    }
}