
## Getting Started

### Prerequisites
- Java 21
- Docker Desktop

### Running the app
Build docker image and start your services:
```
docker-compose up --build
```
Open GraphiQL at `http://localhost:6868/graphiql`.

#### Cleanup
To stop and remove the containers:
```
docker-compose down
```


### Connecting to Postgres
Copy the container ID for the postgres db:
```
docker container ls
```

Connect to postgres using the container ID from above:
```
docker exec -it $CONTAINER_ID psql -U compose-postgres -d graphql-todo
```

#### Example Postgres Commands
List all databases:
```
\l
```

Connect to the `graphql-todo` database:
```
\c graphql-todo
```

List all tables:
```
\dt
```

View columns for the `todos` table:
```
\d+ todos
```