FROM public.ecr.aws/docker/library/amazoncorretto:21-alpine3.18-jdk AS base

FROM base AS builder
WORKDIR /app
COPY .mvn .mvn
COPY mvnw pom.xml ./
RUN --mount=type=bind,source=src,target=/app/src \
	./mvnw -f /app/pom.xml clean package -DskipTests

FROM base AS app
COPY --from=builder /app/target/*.jar application.jar
ENTRYPOINT ["java", "-jar", "/application.jar"]